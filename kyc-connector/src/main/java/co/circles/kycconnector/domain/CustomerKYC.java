package co.circles.kycconnector.domain;

//import jakarta.persistence.PrePersist;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;
import java.util.UUID;

@Document
@Data
public class CustomerKYC {

    @Id
    private UUID id;
    private Map<String,String> props;

    //@PrePersist
    public void prePersist(){
        if(id== null){
            id = UUID.randomUUID();
        }
    }

}
