package co.circles.kycconnector.domain;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerKYCRepository extends MongoRepository<CustomerKYC, String> {
}
