package co.circles.kycconnector.application;

import co.circles.kycconnector.domain.CustomerKYCRepository;
import co.circles.kycconnector.dto.KYCInfo;
import co.circles.kycconnector.external.ExternalAdapter;
import co.circles.kycconnector.api.response.KYCResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KYCService {

    @Autowired
    private ExternalAdapter adapter;

    @Autowired
    private CustomerKYCRepository repository;

    public KYCResponse submitKyc(KYCInfo request) {
         KYCInfo info = adapter.submitKYC(request);
         repository.save(request.toEntity());
         return null;
    }
}
