package co.circles.kycconnector.application;

import co.circles.kycconnector.dto.KycCallBackDTO;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class CallBackHandlerImpl implements CallBackHandler{
    @Override
    public void updateStatus(KycCallBackDTO dto) {
        System.out.println("Received Callback " + dto);
    }
}
