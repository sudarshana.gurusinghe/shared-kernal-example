package co.circles.kycconnector.application;

import co.circles.kycconnector.dto.KycCallBackDTO;

/**
 * Provides methods for callbacks from the connector-extension
 */

public interface CallBackHandler {
    public void updateStatus(KycCallBackDTO dto);

}
