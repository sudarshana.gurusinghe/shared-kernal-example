package co.circles.kycconnector.api.v1;

import co.circles.kycconnector.api.request.KYCRequest;
import co.circles.kycconnector.api.response.KYCResponse;
import co.circles.kycconnector.application.KYCService;
import co.circles.kycconnector.dto.KYCInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/kyc")
public class KYCController {

    @Autowired
    private KYCService kycService;

    @PostMapping
    public @ResponseBody KYCResponse submitKyc(@RequestBody KYCRequest request){
        return kycService.submitKyc(request.toDTO());
    }

    @PutMapping
    public Object updateKyc(Object kyc){
        return null;
    }

    @DeleteMapping
    public Object cancelKyc(Object kyc){
        return null;
    }

    @GetMapping("/{id}")
    public Object getKyc(){
        return null;
    }

}
