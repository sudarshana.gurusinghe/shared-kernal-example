package co.circles.kycconnector.api.request;

import co.circles.kycconnector.dto.KYCInfo;
import lombok.Data;

import java.util.Map;

@Data
public class KYCRequest {

    private String firstName;
    private String lastname;
    private String orderId;
    private String userId;
    private Map<String,String> props;

    public KYCInfo toDTO(){
        KYCInfo info = new KYCInfo();
        info.setFirstName(firstName);
        info.setLastname(lastname);
        info.setOrderId(orderId);
        info.setUserId(userId);
        info.setProps(props);
        return info;
    }

}
