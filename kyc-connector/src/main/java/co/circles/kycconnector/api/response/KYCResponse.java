package co.circles.kycconnector.api.response;

import lombok.Data;

@Data
public class KYCResponse {

    private String kycReference;
}
