package co.circles.kycconnector.external;


import co.circles.kycconnector.dto.KYCInfo;

public interface ExternalAdapter {
    public KYCInfo submitKYC(KYCInfo request);
}
