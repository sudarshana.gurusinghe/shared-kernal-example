package co.circles.kycconnector.dto;

import lombok.Data;

@Data
public class KycCallBackDTO {
    private String kycID;
    private String externalRef;
    private ApprovalStatus status;
}
