package co.circles.kycconnector.dto;

import co.circles.kycconnector.domain.CustomerKYC;
import lombok.Data;

import java.util.Map;
import java.util.UUID;

@Data
public class KYCInfo {
    private String firstName;
    private String lastname;
    private String orderId;
    private String userId;
    private Map<String,String> props;

    public CustomerKYC toEntity(){
        CustomerKYC kyc = new CustomerKYC();
        kyc.setId(UUID.randomUUID());
        kyc.setProps(props);
        return kyc;
    }
}
