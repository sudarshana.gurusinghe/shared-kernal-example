package co.circles.kycconnector.dto;

public enum ApprovalStatus {
    APPROVED,REJECTED,CANCELLED
}
