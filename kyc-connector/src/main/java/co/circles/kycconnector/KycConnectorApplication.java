package co.circles.kycconnector;

import co.circles.kycconnector.domain.CustomerKYC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertCallback;

import java.util.UUID;

@SpringBootApplication
@ComponentScan({"co.circles"})
@Configuration
public class KycConnectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(KycConnectorApplication.class, args);
    }

    @Bean
    public BeforeConvertCallback<CustomerKYC> beforeSaveCallback() {

        return (entity, collection) -> {

            if (entity.getId() == null) {
                entity.setId(UUID.randomUUID());
            }
            return entity;
        };
    }

}
