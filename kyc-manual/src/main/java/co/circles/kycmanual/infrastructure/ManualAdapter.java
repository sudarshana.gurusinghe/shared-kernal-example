package co.circles.kycmanual.infrastructure;
import co.circles.kycconnector.dto.KYCInfo;
import co.circles.kycconnector.external.ExternalAdapter;
import org.springframework.stereotype.Service;

@Service
public class ManualAdapter implements ExternalAdapter {
    @Override
    public KYCInfo submitKYC(KYCInfo request) {
        System.out.println("I am in the manual Adapter");
        return null;
    }
}
