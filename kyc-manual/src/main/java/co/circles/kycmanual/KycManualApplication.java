package co.circles.kycmanual;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@SpringBootApplication
@ComponentScan({"co.circles"})
@Configuration

public class KycManualApplication {

    public static void main(String[] args) {
        SpringApplication.run(KycManualApplication.class, args);
    }

}
