package co.circles.kycmanual.application;

import co.circles.kycconnector.application.CallBackHandler;
import co.circles.kycconnector.dto.ApprovalStatus;
import co.circles.kycconnector.dto.KycCallBackDTO;
import co.circles.kycmanual.dto.KYCDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManualKYCService {

    @Autowired
    private CallBackHandler callBackHandler;

    public String approve(KYCDto obj){
        final KycCallBackDTO cbDto = new KycCallBackDTO();
        cbDto.setKycID(obj.getKycId());
        cbDto.setExternalRef(obj.getExternalRef());
        // TODO : The following mapping should be done based on the call from outside
        cbDto.setStatus(ApprovalStatus.APPROVED);
        callBackHandler.updateStatus(cbDto);
        return null;
    }
}
