package co.circles.kycmanual.dto;

import lombok.Data;

@Data
public class KYCDto {

    private String kycId;
    private String action;
    private String externalRef;
}
