package co.circles.kycmanual.api.request;


import co.circles.kycmanual.dto.KYCDto;
import lombok.Data;

@Data
public class KYCRo {
    private String kycId;
    private String action;
    private String externalRef;

    public KYCDto toDTO(){
        KYCDto dto = new KYCDto();
        dto.setKycId(kycId);
        dto.setAction(action);
        dto.setExternalRef(externalRef);
        return dto;
    }

}
