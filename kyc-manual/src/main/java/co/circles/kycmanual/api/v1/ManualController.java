package co.circles.kycmanual.api.v1;

import co.circles.kycmanual.api.request.KYCRo;
import co.circles.kycmanual.api.response.KycPostResponse;
import co.circles.kycmanual.application.ManualKYCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/v1/kyc/manual/")
public class ManualController {

    @Autowired
    private ManualKYCService kycService;

    @PostMapping
    public @ResponseBody KycPostResponse approve(@RequestBody KYCRo ro){
            kycService.approve(ro.toDTO());
            return null;
    }
}
