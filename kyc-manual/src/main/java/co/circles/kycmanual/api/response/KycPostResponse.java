package co.circles.kycmanual.api.response;

import lombok.Data;

@Data
public class KycPostResponse {
    private String kycId;
}
